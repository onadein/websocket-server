create role accessuser;
alter role accessuser with nosuperuser inherit nocreaterole nocreatedb login password 'q1w2e3' valid until 'infinity';

create database accessserver owner = accessuser;

\connect accessserver

CREATE TABLE users
(
  id serial NOT NULL,
  email text,
  password text,
  
  CONSTRAINT pk_users_id PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);
alter table users owner to accessuser;

CREATE TABLE tokens
(
  id serial NOT NULL,
  user_id integer,
  api_token text,
  api_token_issue_date timestamp,
  api_token_expiration_date timestamp,
  token_active boolean,
  
  CONSTRAINT pk_tokens_id PRIMARY KEY (id),
  CONSTRAINT fk_user_id FOREIGN KEY (user_id) REFERENCES users (id) MATCH SIMPLE ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);
alter table tokens owner to accessuser;

CREATE TABLE messages_log
(
  id serial NOT NULL,
  message_type text,
  message_datetime timestamp,
  sequence_id text,
  error_code integer,
  user_email text,
  message_data text,
  
  CONSTRAINT pk_messages_log_id PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);
alter table messages_log owner to accessuser;

--// Some users for test:
INSERT INTO public.users(email, password) VALUES ('123@gmail.com', 'newPassword');
INSERT INTO public.users(email, password) VALUES ('user@mail.com', 'pasw');
INSERT INTO public.users(email, password) VALUES ('lock@send.com', 'rock');
INSERT INTO public.users(email, password) VALUES ('dummy@bk.com', 'super');


CREATE OR REPLACE FUNCTION make_plpgsql()
RETURNS VOID
LANGUAGE SQL
AS $$
CREATE LANGUAGE plpgsql;
$$;
 
SELECT
    CASE
    WHEN EXISTS(
        SELECT 1
        FROM pg_catalog.pg_language
        WHERE lanname='plpgsql'
    )
    THEN NULL
    ELSE make_plpgsql() END;
 
DROP FUNCTION make_plpgsql();

CREATE OR REPLACE FUNCTION create_token_for_user_id (p_userid IN INTEGER,
				   p_tokenid IN TEXT,
				   p_expiration IN TIMESTAMP)
RETURNS BOOLEAN
AS $create_token_for_user_id$
BEGIN
    IF p_userid > 0
    THEN
	  UPDATE tokens SET token_active=FALSE WHERE user_id = p_userid AND token_active = TRUE;
	  INSERT INTO tokens(user_id, api_token, api_token_issue_date, api_token_expiration_date, token_active)
	  VALUES (p_userid, p_tokenid, CURRENT_TIMESTAMP, p_expiration, TRUE);
	  RETURN TRUE;
    END IF;
    EXCEPTION
       WHEN others THEN
          RETURN FALSE;    
    RETURN FALSE;
END;
$create_token_for_user_id$ LANGUAGE plpgsql;



