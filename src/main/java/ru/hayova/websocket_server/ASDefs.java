package ru.hayova.websocket_server;

public final class ASDefs {
	public final static String MESSAGE_TYPE_LOGIN_CUSTOMER = "LOGIN_CUSTOMER";
	public final static String MESSAGE_TYPE_CUSTOMER_API_TOKEN = "CUSTOMER_API_TOKEN";
	public final static String MESSAGE_TYPE_CUSTOMER_ERROR = "CUSTOMER_ERROR";
	public final static String MESSAGE_TYPE_GENERAL_ERROR = "GENERAL_ERROR";
	
	public final static Integer ERROR_CODE_NO_CUSTOMER_FOUND = 1;
	public final static String ERROR_MESSAGE_NO_CUSTOMER_FOUND = "No customer found";
	
	public final static Integer ERROR_CODE_MESSAGE_ERROR = 2;
	public final static String ERROR_MESSAGE_MESSAGE_ERROR = "Request message error or message is empty";
	
	public final static Integer ERROR_CODE_NO_DATA_PROVIDED = 3;
	public final static String ERROR_MESSAGE_NO_DATA_PROVIDED = "No data part was provided in request";
	
	public final static Integer ERROR_CODE_NO_EMAIL_PROVIDED = 4;
	public final static String ERROR_MESSAGE_NO_EMAIL_PROVIDED = "No email was provided in request";
	
	public final static Integer ERROR_CODE_NO_PASSWORD_PROVIDED = 5;
	public final static String ERROR_MESSAGE_NO_PASSWORD_PROVIDED = "No password was provided in request";
	
	public final static Integer ERROR_CODE_DATASOURCE_NOT_AVAIBLE = 6;
	public final static String ERROR_MESSAGE_DATASOURCE_NOT_AVAIBLE = "Datasource not avaible";
}
