package ru.hayova.websocket_server;

import java.io.IOException;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import ru.hayova.db.Users;

public class ASUtils {
	
    @SuppressWarnings("rawtypes")
    public static AccessMessage stringToMessage(String message, Class viewClass) {
    	ObjectMapper mapper = new ObjectMapper();

		try {
			return mapper.readerWithView(viewClass).forType(AccessMessage.class).readValue(message);
		} catch (JsonGenerationException e) {
			e.printStackTrace();
		} catch (JsonMappingException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return null;
    }
    
    @SuppressWarnings("rawtypes")
    public static String messageToString(AccessMessage accessMessage, Class viewClass) {
    	ObjectMapper mapper = new ObjectMapper();
    	
    	try {
			return mapper.writerWithView(viewClass).writeValueAsString(accessMessage);
		} catch (JsonGenerationException e) {
			e.printStackTrace();
		} catch (JsonMappingException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
    	return null;
    }
    
    public static Users findUserInDB(String email, String password) {
    	org.hibernate.Session session = HibernateUtil.getSessionFactory().openSession();
		try {
			org.hibernate.Criteria criteria = session.createCriteria(Users.class);
			criteria.add(org.hibernate.criterion.Restrictions.eq("email", email));
			criteria.add(org.hibernate.criterion.Restrictions.eq("password", password));
			
			criteria.setMaxResults(1);

			return (Users) criteria.uniqueResult();	
		} catch (org.hibernate.HibernateException e) {
			e.printStackTrace();
		} finally {
			session.close();
		}
		
		return null;
    }
    
}
