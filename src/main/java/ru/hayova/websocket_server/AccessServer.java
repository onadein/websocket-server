package ru.hayova.websocket_server;

import javax.naming.InitialContext;
import javax.sql.DataSource;
import javax.websocket.CloseReason;
import javax.websocket.OnClose;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import javax.websocket.server.ServerEndpoint;

import ru.hayova.db.Tokens;
import ru.hayova.db.Users;

@ServerEndpoint("/accessServer")
public class AccessServer {

    @OnMessage
    public String onMessage(String message) {
    	LogsProvider.writeMessageToLog(message);
    	String answer = parseMessage(message);
    	LogsProvider.writeMessageToLog(answer);
        return answer;
    }

    @OnOpen
    public void onOpen(Session session) {
        //TODO: session.getId()
    }
    
    @OnClose
    public void onClose(CloseReason reason) {
        //TODO: reason.getReasonPhrase()
    }
    
    private boolean testDatasource() {
    	try {
    	    InitialContext ctx = new InitialContext();
    	    DataSource ds = (DataSource) ctx.lookup("java:/access");
    	    ds.getConnection().close();
    	    return true;
    	} catch (Exception e) {
    	    e.printStackTrace();
    	}
    	return false;
    }
    
    private String parseMessage(String message) {
    	String strAnswer = null;
    	
    	if(message != null && !message.isEmpty()) {
    	
	    	AccessMessage incomingMessage = ASUtils.stringToMessage(message, Views.Incoming.class);
	    	
	    	if(incomingMessage != null && incomingMessage.getType() != null && incomingMessage.getType().equalsIgnoreCase(ASDefs.MESSAGE_TYPE_LOGIN_CUSTOMER))
	    		strAnswer = processLoginMessage(incomingMessage);
	    	else
	    		strAnswer = prepareErrorAnswer(null, ASDefs.MESSAGE_TYPE_GENERAL_ERROR, ASDefs.ERROR_CODE_MESSAGE_ERROR, ASDefs.ERROR_MESSAGE_MESSAGE_ERROR);
    	
    	} else
    		strAnswer = prepareErrorAnswer(null, ASDefs.MESSAGE_TYPE_GENERAL_ERROR, ASDefs.ERROR_CODE_MESSAGE_ERROR, ASDefs.ERROR_MESSAGE_MESSAGE_ERROR);
    	
    	return strAnswer;
    }
    
    private String processLoginMessage(AccessMessage accessMessage) {
    	String strAnswer = null;
    	
    	if(!testDatasource()) {
    		return prepareErrorAnswer(accessMessage.getSequence_id(), ASDefs.MESSAGE_TYPE_GENERAL_ERROR, ASDefs.ERROR_CODE_DATASOURCE_NOT_AVAIBLE, ASDefs.ERROR_MESSAGE_DATASOURCE_NOT_AVAIBLE);
    	}
    	
    	if(accessMessage.getData() != null) {
    		if(accessMessage.getData().getEmail() != null && !accessMessage.getData().getEmail().isEmpty()) {
    			if(accessMessage.getData().getPassword() != null && !accessMessage.getData().getPassword().isEmpty()) {
    				Users dbUser = ASUtils.findUserInDB(accessMessage.getData().getEmail(), accessMessage.getData().getPassword());
    				if(dbUser != null) {
    					Tokens newToken = TokenCreator.createTokenForUser(dbUser);
    					if(newToken != null) {
							AccessMessage answerMessage = new AccessMessage();
							answerMessage.setType(ASDefs.MESSAGE_TYPE_CUSTOMER_API_TOKEN);
							if(accessMessage.getSequence_id() != null)
								answerMessage.setSequence_id(accessMessage.getSequence_id());
							AccessMessage.Data data = answerMessage.new Data();
							data.setApi_token(newToken.getApiToken());
							data.setApi_token_expiration_date(newToken.getApiTokenExpirationDate());
							answerMessage.setData(data);
							
							strAnswer = ASUtils.messageToString(answerMessage, Views.Success.class);
    					}
    				} else
            			strAnswer = prepareErrorAnswer(accessMessage.getSequence_id(), ASDefs.MESSAGE_TYPE_CUSTOMER_ERROR, ASDefs.ERROR_CODE_NO_CUSTOMER_FOUND, ASDefs.ERROR_MESSAGE_NO_CUSTOMER_FOUND);
        		} else
        			strAnswer = prepareErrorAnswer(accessMessage.getSequence_id(), ASDefs.MESSAGE_TYPE_GENERAL_ERROR, ASDefs.ERROR_CODE_NO_PASSWORD_PROVIDED, ASDefs.ERROR_MESSAGE_NO_PASSWORD_PROVIDED);
    		} else
    			strAnswer = prepareErrorAnswer(accessMessage.getSequence_id(), ASDefs.MESSAGE_TYPE_GENERAL_ERROR, ASDefs.ERROR_CODE_NO_EMAIL_PROVIDED, ASDefs.ERROR_MESSAGE_NO_EMAIL_PROVIDED);
    	} else
    		strAnswer = prepareErrorAnswer(accessMessage.getSequence_id(), ASDefs.MESSAGE_TYPE_GENERAL_ERROR, ASDefs.ERROR_CODE_NO_DATA_PROVIDED, ASDefs.ERROR_MESSAGE_NO_DATA_PROVIDED);
    	
    	return strAnswer;
    }
       
    private String prepareErrorAnswer(String sequence_id, String type, Integer error_code, String error_description) {
    	AccessMessage errorAnswer = new AccessMessage();    	
    	errorAnswer.setType(type);
    	if(sequence_id != null)
    		errorAnswer.setSequence_id(sequence_id);
    	AccessMessage.Data data = errorAnswer.new Data();
    	data.setError_code(error_code);
    	data.setError_description(error_description);
    	errorAnswer.setData(data);
    	return ASUtils.messageToString(errorAnswer, Views.Error.class);
    }
    
} 