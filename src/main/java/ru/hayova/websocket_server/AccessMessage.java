package ru.hayova.websocket_server;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonView;

public class AccessMessage {
	
	@JsonView(Views.Normal.class)
	private String type;
	
	@JsonView(Views.Normal.class)
	private String sequence_id;
	
	@JsonView(Views.Normal.class)
	private Data data;
	
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}

	public String getSequence_id() {
		return sequence_id;
	}
	public void setSequence_id(String sequence_id) {
		this.sequence_id = sequence_id;
	}

	public Data getData() {
		return data;
	}
	public void setData(Data data) {
		this.data = data;
	}
	
	public class Data {
		@JsonView(Views.Incoming.class)
		private String email;
		
		@JsonView(Views.Incoming.class)
		private String password;
		
		@JsonView(Views.Success.class)
		private String api_token;
		
		@JsonView(Views.Success.class)
		@JsonFormat(shape=JsonFormat.Shape.STRING, pattern="yyyy-MM-dd'T'HH:mm:ss'Z'")
		private Date api_token_expiration_date;

		@JsonView(Views.Error.class)
		private String error_description;
		
		@JsonView(Views.Error.class)
		private Integer error_code;
		
		public String getEmail() {
			return email;
		}
		public void setEmail(String email) {
			this.email = email;
		}
		
		public String getPassword() {
			return password;
		}
		public void setPassword(String password) {
			this.password = password;
		}
		
		public String getApi_token() {
			return api_token;
		}
		public void setApi_token(String api_token) {
			this.api_token = api_token;
		}
		
		public Date getApi_token_expiration_date() {
			return api_token_expiration_date;
		}
		public void setApi_token_expiration_date(Date api_token_expiration_date) {
			this.api_token_expiration_date = api_token_expiration_date;
		}
		
		public String getError_description() {
			return error_description;
		}
		public void setError_description(String error_description) {
			this.error_description = error_description;
		}
		
		public Integer getError_code() {
			return error_code;
		}
		public void setError_code(Integer error_code) {
			this.error_code = error_code;
		}
	}            	
}
