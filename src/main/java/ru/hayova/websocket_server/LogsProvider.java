package ru.hayova.websocket_server;

import java.util.Date;

import ru.hayova.db.MessagesLog;

public class LogsProvider {
	
    public static void writeMessageToLog(String message) {
    	if(message != null && !message.isEmpty()) {
	    	org.hibernate.Session session = HibernateUtil.getSessionFactory().openSession();
			try {
				session.beginTransaction();
		    	MessagesLog msgLog = new MessagesLog();
		        
		    	msgLog.setMessageData(message);
		    	msgLog.setMessageDatetime(new Date());
				
		    	AccessMessage accessMessage = ASUtils.stringToMessage(message, Views.Normal.class);
		    	
		    	if(accessMessage != null) {
		    		if(accessMessage.getSequence_id() != null)
		    			msgLog.setSequenceId(accessMessage.getSequence_id());
		    		if(accessMessage.getType() != null)
		    			msgLog.setMessageType(accessMessage.getType());
	
		    		if(accessMessage.getData() != null){
		    			if(accessMessage.getData().getError_code() != null)
		    				msgLog.setErrorCode(accessMessage.getData().getError_code());
		    			if(accessMessage.getData().getEmail() != null)
		    				msgLog.setUserEmail(accessMessage.getData().getEmail());
		    		}
		    	}
		    	
		    	session.save(msgLog);
		    	session.getTransaction().commit();
			} catch (org.hibernate.HibernateException e) {
				if(session.getTransaction()!=null&&session.getTransaction().isActive())
					session.getTransaction().rollback();
				e.printStackTrace();
			} catch (Exception e) {
				if(session.getTransaction()!=null&&session.getTransaction().isActive())
					session.getTransaction().rollback();
				e.printStackTrace();
			} finally {
	    		session.close();
	    	}
    	}
    }
    
}
