package ru.hayova.websocket_server;

public class Views {
	public static class Normal{};
	
	public static class Incoming extends Normal{};
	
	public static class Error extends Normal{};
	
	public static class Success extends Normal{};
}
