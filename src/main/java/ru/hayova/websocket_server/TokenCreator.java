package ru.hayova.websocket_server;

import java.util.Calendar;
import java.util.UUID;

import ru.hayova.db.Tokens;

public class TokenCreator {
	
    public static Tokens createTokenForUser(ru.hayova.db.Users user) {
    	
    	org.hibernate.Session session = HibernateUtil.getSessionFactory().openSession();
		try {
	    	Tokens newToken = new Tokens();
	    	
			newToken.setApiToken(UUID.randomUUID().toString());
			Calendar expireDate = Calendar.getInstance();
			newToken.setApiTokenIssueDate(expireDate.getTime());
			expireDate.add(Calendar.HOUR, 2);
			newToken.setApiTokenExpirationDate(expireDate.getTime());
			newToken.setTokenActive(true);
			newToken.setUsers(user);

    		Boolean result = (Boolean)session.createSQLQuery("{? = call create_token_for_user_id (:userid, :tokenid, :expiration)}")
    				.setLong("userid", newToken.getUsers().getId())
    				.setString("tokenid", newToken.getApiToken())
    				.setTimestamp("expiration", newToken.getApiTokenExpirationDate())
    				.uniqueResult();
    		
    		if(result)
    			return newToken;
		} catch (org.hibernate.HibernateException e) {
			if(session.getTransaction()!=null&&session.getTransaction().isActive())
				session.getTransaction().rollback();
			e.printStackTrace();
		} catch (Exception e) {
			if(session.getTransaction()!=null&&session.getTransaction().isActive())
				session.getTransaction().rollback();
			e.printStackTrace();
		} finally {
    		session.close();
    	}
		
		return null;
    }
 
}
