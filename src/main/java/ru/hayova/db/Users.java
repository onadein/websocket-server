package ru.hayova.db;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "users", schema = "public")
public class Users implements java.io.Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private int id;
	private String email;
	private String password;
	private Set<Tokens> tokenses = new HashSet<Tokens>(0);

	public Users() {
	}

	public Users(int id) {
		this.id = id;
	}

	public Users(int id, String email, String password, Set<Tokens> tokenses) {
		this.id = id;
		this.email = email;
		this.password = password;
		this.tokenses = tokenses;
	}

	@Id
	@Column(name = "id", unique = true, nullable = false)
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "users_id_seq_gen")
	@SequenceGenerator(name = "users_id_seq_gen", sequenceName = "users_id_seq")
	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	@Column(name = "email")
	public String getEmail() {
		return this.email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	@Column(name = "password")
	public String getPassword() {
		return this.password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	@OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL, mappedBy = "users")
	public Set<Tokens> getTokenses() {
		return this.tokenses;
	}

	public void setTokenses(Set<Tokens> tokenses) {
		this.tokenses = tokenses;
	}

}
