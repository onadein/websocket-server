package ru.hayova.db;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "messages_log", schema = "public")
public class MessagesLog implements java.io.Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private int id;
	private String messageType;
	private Date messageDatetime;
	private String sequenceId;
	private Integer errorCode;
	private String userEmail;
	private String messageData;

	public MessagesLog() {
	}

	public MessagesLog(int id) {
		this.id = id;
	}

	public MessagesLog(int id, String messageType, Date messageDatetime, String sequenceId, Integer errorCode,
			String userEmail, String messageData) {
		this.id = id;
		this.messageType = messageType;
		this.messageDatetime = messageDatetime;
		this.sequenceId = sequenceId;
		this.errorCode = errorCode;
		this.userEmail = userEmail;
		this.messageData = messageData;
	}

	@Id
	@Column(name = "id", unique = true, nullable = false)
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "messages_log_id_seq_gen")
	@SequenceGenerator(name = "messages_log_id_seq_gen", sequenceName = "messages_log_id_seq")
	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	@Column(name = "message_type")
	public String getMessageType() {
		return this.messageType;
	}

	public void setMessageType(String messageType) {
		this.messageType = messageType;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "message_datetime", length = 29)
	public Date getMessageDatetime() {
		return this.messageDatetime;
	}

	public void setMessageDatetime(Date messageDatetime) {
		this.messageDatetime = messageDatetime;
	}

	@Column(name = "sequence_id")
	public String getSequenceId() {
		return this.sequenceId;
	}

	public void setSequenceId(String sequenceId) {
		this.sequenceId = sequenceId;
	}

	@Column(name = "error_code")
	public Integer getErrorCode() {
		return this.errorCode;
	}

	public void setErrorCode(Integer errorCode) {
		this.errorCode = errorCode;
	}

	@Column(name = "user_email")
	public String getUserEmail() {
		return this.userEmail;
	}

	public void setUserEmail(String userEmail) {
		this.userEmail = userEmail;
	}

	@Column(name = "message_data")
	public String getMessageData() {
		return this.messageData;
	}

	public void setMessageData(String messageData) {
		this.messageData = messageData;
	}

}
