package ru.hayova.db;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "tokens", schema = "public")
public class Tokens implements java.io.Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private int id;
	private Users users;
	private String apiToken;
	private Date apiTokenIssueDate;
	private Date apiTokenExpirationDate;
	private Boolean tokenActive;

	public Tokens() {
	}

	public Tokens(int id) {
		this.id = id;
	}

	public Tokens(int id, Users users, String apiToken, Date apiTokenIssueDate, Date apiTokenExpirationDate,
			Boolean tokenActive) {
		this.id = id;
		this.users = users;
		this.apiToken = apiToken;
		this.apiTokenIssueDate = apiTokenIssueDate;
		this.apiTokenExpirationDate = apiTokenExpirationDate;
		this.tokenActive = tokenActive;
	}

	@Id
	@Column(name = "id", unique = true, nullable = false)
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "tokens_id_seq_gen")
	@SequenceGenerator(name = "tokens_id_seq_gen", sequenceName = "tokens_id_seq")
	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "user_id")
	public Users getUsers() {
		return this.users;
	}

	public void setUsers(Users users) {
		this.users = users;
	}

	@Column(name = "api_token")
	public String getApiToken() {
		return this.apiToken;
	}

	public void setApiToken(String apiToken) {
		this.apiToken = apiToken;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "api_token_issue_date", length = 29)
	public Date getApiTokenIssueDate() {
		return this.apiTokenIssueDate;
	}

	public void setApiTokenIssueDate(Date apiTokenIssueDate) {
		this.apiTokenIssueDate = apiTokenIssueDate;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "api_token_expiration_date", length = 29)
	public Date getApiTokenExpirationDate() {
		return this.apiTokenExpirationDate;
	}

	public void setApiTokenExpirationDate(Date apiTokenExpirationDate) {
		this.apiTokenExpirationDate = apiTokenExpirationDate;
	}

	@Column(name = "token_active")
	public Boolean getTokenActive() {
		return this.tokenActive;
	}

	public void setTokenActive(Boolean tokenActive) {
		this.tokenActive = tokenActive;
	}

}
